$(function () {

    $.validator.addMethod("PWCHECK",
        function(value,element) {
            if (/^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@%*?&#'"*&/]){1,}).{8,}$/.test(value)) {
                return true;
            } else {
                return false;
            }
        });

    $("#inscription_form").validate(
        {
            rules:{
                nom_per:{
                    required: true,
                    minlength: 2
                },
                prenom_per:{
                    required: true,
                    minlength: 2
                },
                email_per: "required",
                password:{
                    required: true,
                    PWCHECK: true
                },
                password_conf:{
                    required: true,
                    equalTo: "#password"
                }
            },
            messages:{
                nom_per:{
                    required: "Veuillez saisir votre nom",
                    minlength: "Veuillez entrer un nom de plus d'un caractère"
                },
                prenom_per:{
                    required: "Veuillez saisir votre prenom",
                    minlength: "Veuillez entrer un prenom de plus d'un caractère"
                },
                email_per: "Veuillez saisir une adresse e-mail valide",
                password:{
                    required: "Veuillez saisir un mot de passe",
                    PWCHECK: "Le mot de passe doit contenir au moins 8 caractères, dont une majuscule, une minuscule, un chiffre et un caractère spécial."
                },
                password_conf:{
                    required: "Veuillez confirmer le mot de passe",
                    equalTo: "La confirmation doit être égale au mot de passe"
                },
            }
        }
    );
});