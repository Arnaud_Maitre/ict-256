$(function () { // créer une fonction qui se lance uniquement une fois que la page est totalement chargée     (anciennement, il fallait faire $(document.ready(function (){ ... et même chose pour la suite
    var page_originale = true;

    $("img").on("click", function () {
        alert("Hello World");
        if (page_originale) {
            $("#content").css("background", "grey").css("color", "black");
            $("h1,h2:not(\".anecdote\")").css("color", "#aa0000").css("border-color", "#aa0000");
            $("a:first-child").css("color", "blue");
            page_originale = false;
        } else {
            location.reload();
            page_originale = true;
        }
    });

});

$(function (){
    var vitesseClignotement = 2000;
    for (var i = 0; i < 40; i++) {
        $("h2").fadeIn(vitesseClignotement);
        $("h2").fadeOut(vitesseClignotement);
    }
    $("h2").fadeIn(vitesseClignotement);
});